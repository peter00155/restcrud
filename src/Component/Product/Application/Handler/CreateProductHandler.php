<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Handler;

use App\Component\Product\Domain\Event\ProductCreatedEvent;
use App\Component\Product\Application\Factory\ProductFactoryInterface;
use App\Component\Product\Application\Message\CreateProductCommand;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateProductHandler
{
    private ProductRepositoryInterface $productRepository;

    private ProductFactoryInterface $productFactory;

    private EventDispatcherInterface $dispatcher;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ProductFactoryInterface $productFactory,
        EventDispatcherInterface $dispatcher
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(CreateProductCommand $command): void
    {
        $this->productRepository->saveProduct(
            $product = $this->productFactory->fromDTO($command->getProductDTO())
        );

        $event = new ProductCreatedEvent($product);
        $this->dispatcher->dispatch($event);
    }
}