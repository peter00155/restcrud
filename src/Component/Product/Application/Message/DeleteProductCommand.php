<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Message;

use App\Component\Product\Domain\Entity\Product;

class DeleteProductCommand
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}