<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Handler;

use App\Component\Product\Application\Message\GetProductListQuery;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;

class GetProductListHandler
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(GetProductListQuery $query): void
    {
        $products = $this->productRepository->getProducts(
            $query->getRequestListDTO()->getOffset(),
            $query->getRequestListDTO()->getLimit(),
            $query->getRequestListDTO()->getSortColumn(),
            $query->getRequestListDTO()->getSortOrder(),
        );

        $query->setView($products);
    }
}