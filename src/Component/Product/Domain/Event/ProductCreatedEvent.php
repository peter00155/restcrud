<?php

declare(strict_types=1);

namespace App\Component\Product\Domain\Event;

use App\Component\Product\Domain\Entity\Product;
use Symfony\Contracts\EventDispatcher\Event;

class ProductCreatedEvent extends Event
{
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}