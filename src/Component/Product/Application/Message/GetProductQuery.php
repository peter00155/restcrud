<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Message;

use App\Component\Product\Domain\Entity\Product;

class GetProductQuery
{
    private int $productId;

    private ?Product $view = null;

    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }

    public function getProductId(): int
    {
        return $this->productId;
    }

    public function setView(Product $product): void
    {
        $this->view = $product;
    }

    public function getView(): ?Product
    {
        return $this->view;
    }
}