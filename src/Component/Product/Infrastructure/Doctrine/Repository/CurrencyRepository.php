<?php

namespace App\Component\Product\Infrastructure\Doctrine\Repository;

use App\Component\Product\Domain\Entity\Currency;
use App\Component\Product\Domain\Repository\CurrencyRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CurrencyRepository extends ServiceEntityRepository implements CurrencyRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Currency::class);
    }

    public function getCurrencies(): array
    {
        return $this->findAll();
    }

    public function getCurrency(int $id): Currency
    {
        return $this->find($id);
    }
}
