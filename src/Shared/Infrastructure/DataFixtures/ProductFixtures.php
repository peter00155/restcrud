<?php

namespace App\Shared\Infrastructure\DataFixtures;

use App\Component\Product\Domain\Entity\Currency;
use App\Component\Product\Domain\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $currency = new Currency();
        $currency->setCode('PLN');
        $manager->persist($currency);

        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName($faker->name);
            $product->setDescription($faker->sentence);
            $product->setPrice($faker->randomDigit);
            $product->setCurrency($currency);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
