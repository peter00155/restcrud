<?php

declare(strict_types=1);

namespace App\Shared\Application\Symfony\Request\Factory;

use App\Shared\Application\Symfony\Request\DTO\RequestListDTO;
use App\Shared\Application\Symfony\Request\ValueObject\Paginator;
use App\Shared\Application\Symfony\Request\ValueObject\Sorter;
use Symfony\Component\HttpFoundation\Request;

class RequestListDTOFactory implements RequestListDTOFactoryInterface
{
    public function fromRequest(Request $request, array $allowedColumns): RequestListDTO
    {
        $headers = $request->headers;
        $paginatorVO = new Paginator((int) $headers->get('page'), (int) $headers->get('limit'));
        $sorterVO = new Sorter($headers->get('sort'), $allowedColumns);

        $dto = new RequestListDTO();
        $dto->setLimit($paginatorVO->getLimit());
        $dto->setOffset($paginatorVO->getOffset());
        $dto->setSortColumn($sorterVO->getColumn());
        $dto->setSortOrder($sorterVO->getOrder());

        return $dto;
    }
}