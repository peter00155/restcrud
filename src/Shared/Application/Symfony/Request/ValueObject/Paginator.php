<?php

declare(strict_types=1);

namespace App\Shared\Application\Symfony\Request\ValueObject;

use App\Shared\Application\Symfony\Request\DTO\RequestListDTO;

class Paginator
{
    private ?int $offset;

    private ?int $limit;

    public function __construct(?int $page, ?int $limit)
    {
        $this->setLimit($limit);
        $this->setOffset($page);
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    private function setLimit(?int $limit): void
    {
        if ($limit > 0) {
            $this->limit = $limit;
        } else {
            $this->limit = RequestListDTO::DEFAULT_LIMIT;
        }
    }

    private function setOffset(?int $page): void
    {
        if ($page > 0) {
            $this->offset = ($page - 1) * $this->getLimit();
        } else {
            $this->offset = RequestListDTO::DEFAULT_OFFSET;
        }
    }
}