<?php

namespace spec\App\Shared\Application\Symfony\Request\Factory;

use App\Shared\Application\Symfony\Request\DTO\RequestListDTO;
use App\Shared\Application\Symfony\Request\Factory\RequestListDTOFactory;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\Request;

class RequestListDTOFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RequestListDTOFactory::class);
    }

    function it_should_return_dto_with_default_values()
    {
        $request = new Request();

        /** @var RequestListDTO $dto */
        $dto = $this->fromRequest($request, ['name']);

        $dto->getLimit()->shouldReturn(RequestListDTO::DEFAULT_LIMIT);
        $dto->getOffset()->shouldReturn(RequestListDTO::DEFAULT_OFFSET);
        $dto->getSortColumn()->shouldReturn(RequestListDTO::DEFAULT_SORT_COLUMN);
        $dto->getSortOrder()->shouldReturn(RequestListDTO::DEFAULT_SORT_ORDER);
    }

    function it_should_return_dto_with_custom_values()
    {
        $request = new Request();
        $request->headers->set('page', 2);
        $request->headers->set('limit', 5);
        $request->headers->set('sort', 'price.desc');

        /** @var RequestListDTO $dto */
        $dto = $this->fromRequest($request, ['price']);

        $dto->getLimit()->shouldReturn(5);
        $dto->getOffset()->shouldReturn(5);
        $dto->getSortColumn()->shouldReturn('price');
        $dto->getSortOrder()->shouldReturn('desc');
    }

    function it_should_change_dto_to_default_values()
    {
        $request = new Request();
        $request->headers->set('page', -1);
        $request->headers->set('limit', 'asd');
        $request->headers->set('sort', 'price');

        /** @var RequestListDTO $dto */
        $dto = $this->fromRequest($request, ['name']);

        $dto->getLimit()->shouldReturn(RequestListDTO::DEFAULT_LIMIT);
        $dto->getOffset()->shouldReturn(RequestListDTO::DEFAULT_OFFSET);
        $dto->getSortColumn()->shouldReturn(RequestListDTO::DEFAULT_SORT_COLUMN);
        $dto->getSortOrder()->shouldReturn(RequestListDTO::DEFAULT_SORT_ORDER);
    }
}
