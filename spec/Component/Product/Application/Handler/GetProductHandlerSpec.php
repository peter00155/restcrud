<?php

namespace spec\App\Component\Product\Application\Handler;

use App\Component\Product\Application\Exception\ProductNotFoundException;
use App\Component\Product\Application\Handler\GetProductHandler;
use App\Component\Product\Application\Message\GetProductQuery;
use App\Component\Product\Domain\Entity\Product;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;
use PhpSpec\ObjectBehavior;

class GetProductHandlerSpec extends ObjectBehavior
{
    private $productRepository;

    function it_is_initializable()
    {
        $this->shouldHaveType(GetProductHandler::class);
    }

    function let(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;

        $this->beConstructedWith($productRepository);
    }

    function it_should_throw_exception_product_not_found()
    {
        $query = new GetProductQuery(1);
        $this->productRepository->getProduct($query->getProductId())->willReturn(null);

        $this->shouldThrow(ProductNotFoundException::class)->during('__invoke', ['query' => $query]);
    }

    function it_should_proccess_without_exception()
    {
        $productDTO = new Product();
        $query = new GetProductQuery(1);
        $this->productRepository->getProduct($query->getProductId())->willReturn($productDTO);

        $this($query);

        $this->shouldNotThrow(ProductNotFoundException::class);
    }
}
