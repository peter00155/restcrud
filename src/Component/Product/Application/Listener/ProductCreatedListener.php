<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Listener;

use App\Component\Product\Domain\Event\ProductCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ProductCreatedListener implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductCreatedEvent::class => 'sendEmail',
        ];
    }

    public function sendEmail(ProductCreatedEvent $event): void
    {
        try {
            $email = (new Email())
                ->from('me@example.com')
                ->to('fake@example.com')
                ->subject('Your product was created')
                ->text(sprintf('Product: %s, have just created', $event->getProduct()->getName()));

            $this->mailer->send($email);
        } catch (TransportException $transportException) {}
    }
}