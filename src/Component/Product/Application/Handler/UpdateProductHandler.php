<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Handler;

use App\Component\Product\Application\Factory\ProductFactoryInterface;
use App\Component\Product\Application\Message\UpdateProductCommand;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;

class UpdateProductHandler
{
    private ProductRepositoryInterface $productRepository;
    private ProductFactoryInterface $productFactory;

    public function __construct(ProductRepositoryInterface $productRepository, ProductFactoryInterface $productFactory)
    {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
    }

    public function __invoke(UpdateProductCommand $command): void
    {
        $this->productRepository->saveProduct(
            $this->productFactory->fromDTO($command->getProductDTO())
        );
    }
}