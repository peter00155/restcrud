<?php

namespace App\Component\Product\Infrastructure\Doctrine\Repository;

use App\Component\Product\Domain\Entity\Product;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getProduct(int $id): ?Product
    {
        return $this->find($id);
    }

    public function getProducts(int $offset, int $limit, string $column, string $order): array
    {
        $alias = 'p';
        return $this->createQueryBuilder($alias)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy("$alias.$column", $order)
            ->getQuery()
            ->getResult()
            ;
    }

    public function deleteProduct(Product $product): void
    {
        $this->_em->remove($product);
        $this->_em->flush();
    }

    public function saveProduct(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }
}
