<?php

declare(strict_types=1);

namespace App\Shared\Application\Symfony\Request\DTO;

class RequestListDTO
{
    public const DEFAULT_OFFSET = 0;

    public const DEFAULT_LIMIT = 10;

    public const DEFAULT_SORT_COLUMN = 'name';

    public const DEFAULT_SORT_ORDER = 'ASC';

    private int $offset = self::DEFAULT_OFFSET;

    private int $limit = self::DEFAULT_LIMIT;

    private string $sortColumn = self::DEFAULT_SORT_COLUMN;

    private string $sortOrder = self::DEFAULT_SORT_ORDER;

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    public function getSortOrder(): string
    {
        return $this->sortOrder;
    }

    public function setSortOrder(string $sortOrder): void
    {
        $this->sortOrder = $sortOrder;
    }
}