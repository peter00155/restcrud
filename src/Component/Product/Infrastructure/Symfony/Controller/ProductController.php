<?php

declare(strict_types=1);

namespace App\Component\Product\Infrastructure\Symfony\Controller;

use App\Component\Product\Application\Message\CreateProductCommand;
use App\Component\Product\Application\Message\DeleteProductCommand;
use App\Component\Product\Application\Message\GetProductListQuery;
use App\Component\Product\Application\Message\GetProductQuery;
use App\Component\Product\Application\Message\UpdateProductCommand;
use App\Component\Product\Application\DTO\ProductDTO;
use App\Component\Product\Infrastructure\Symfony\Form\ProductType;
use App\Shared\Application\Symfony\Request\Factory\RequestListDTOFactoryInterface;
use App\Shared\Infrastructure\Symfony\Security\ParameterTokenAuthVoter;
use App\Shared\Infrastructure\Symfony\Serializer\FormErrorSerializerInterface;
use SimpleBus\SymfonyBridge\Bus\CommandBus;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ProductController
{
    private CommandBus $commandBus;

    private FormFactoryInterface $formFactory;

    private AuthorizationCheckerInterface $authorizationChecker;
    
    private FormErrorSerializerInterface $formErrorSerializer;

    private RequestListDTOFactoryInterface $requestListDTOFactory;

    public function __construct(
        CommandBus $commandBus,
        FormFactoryInterface $formFactory,
        AuthorizationCheckerInterface $authorizationChecker,
        FormErrorSerializerInterface $formErrorSerializer,
        RequestListDTOFactoryInterface $requestListDTOFactory
    ) {
        $this->commandBus = $commandBus;
        $this->formFactory = $formFactory;
        $this->authorizationChecker = $authorizationChecker;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->requestListDTOFactory = $requestListDTOFactory;
    }

    public function get(int $id): JsonResponse
    {
        $message = new GetProductQuery($id);
        $this->commandBus->handle($message);

        return new JsonResponse($message->getView());
    }

    public function list(Request $request): JsonResponse
    {
        $requestListDTO = $this->requestListDTOFactory->fromRequest($request, ['name', 'price']);
        $message = new GetProductListQuery($requestListDTO);
        $this->commandBus->handle($message);

        return new JsonResponse($message->getView());
    }

    public function create(Request $request): JsonResponse
    {
        $this->authorizationChecker->isGranted(ParameterTokenAuthVoter::API_ACCESS);

        $form = $this->formFactory->create(ProductType::class, ProductDTO::createNew());
        $form->handleRequest($request);
        $form->submit(json_decode($request->getContent(), true));
        if (!$form->isValid()) {
            return new JsonResponse(
                ['errors' => $this->formErrorSerializer->convertFormToArray($form)],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $message = new CreateProductCommand($form->getData());
        $this->commandBus->handle($message);

        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    public function update(Request $request, int $id): JsonResponse
    {
        $this->authorizationChecker->isGranted(ParameterTokenAuthVoter::API_ACCESS);

        $message = new GetProductQuery($id);
        $this->commandBus->handle($message);

        $form = $this->formFactory->create(ProductType::class, ProductDTO::fromEntity($message->getView()));
        $form->handleRequest($request);
        $form->submit(json_decode($request->getContent(), true));
        if (!$form->isValid()) {
            return new JsonResponse(
                ['errors' => $this->formErrorSerializer->convertFormToArray($form)],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $command = new UpdateProductCommand($form->getData());
        $this->commandBus->handle($command);

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    public function delete(int $id): JsonResponse
    {
        $this->authorizationChecker->isGranted(ParameterTokenAuthVoter::API_ACCESS);

        $message = new GetProductQuery($id);
        $this->commandBus->handle($message);

        $command = new DeleteProductCommand($message->getView());
        $this->commandBus->handle($command);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}