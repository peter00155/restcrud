<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Exception;

use Exception;

class ProductNotFoundException extends Exception
{

}