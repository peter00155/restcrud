<?php

declare(strict_types=1);

namespace App\Component\Product\Application\DTO;

use App\Component\Product\Domain\Entity\Product;

class ProductDTO
{
    private ?int $id = null;

    private ?string $name;

    private ?string $description;

    private ?int $price;

    private ?CurrencyDTO $currency;

    private function __construct() {}

    public static function createNew(): self
    {
        return new static();
    }

    public static function fromEntity(Product $product): self
    {
        $currencyDto = CurrencyDTO::create(
            $product->getCurrency()->getId(),
            $product->getCurrency()->getCode(),
        );

        $dto = new static();
        $dto->setId($product->getId());
        $dto->setName($product->getName());
        $dto->setDescription($product->getDescription());
        $dto->setPrice($product->getPrice());
        $dto->setCurrency($currencyDto);

        return $dto;
    }

    public function toArray(): array
    {
        $data['id'] = $this->getId();
        $data['name'] = $this->getName();
        $data['description'] = $this->getDescription();
        $data['price'] = $this->getPrice();
        $data['currency'] = $this->getCurrency()->getCode();

        return $data;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): void
    {
        $this->price = $price;
    }

    public function getCurrency(): ?CurrencyDTO
    {
        return $this->currency;
    }

    public function setCurrency(?CurrencyDTO $currency): void
    {
        $this->currency = $currency;
    }
}