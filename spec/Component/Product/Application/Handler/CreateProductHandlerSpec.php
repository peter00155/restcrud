<?php

namespace spec\App\Component\Product\Application\Handler;

use App\Component\Product\Application\DTO\ProductDTO;
use App\Component\Product\Application\Factory\ProductFactoryInterface;
use App\Component\Product\Application\Handler\CreateProductHandler;
use App\Component\Product\Application\Message\CreateProductCommand;
use App\Component\Product\Domain\Entity\Product;
use App\Component\Product\Domain\Event\ProductCreatedEvent;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;
use PhpSpec\ObjectBehavior;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateProductHandlerSpec extends ObjectBehavior
{
    private $dispatcher;

    private $productFactory;

    function it_is_initializable()
    {
        $this->shouldHaveType(CreateProductHandler::class);
    }

    function let(
        ProductRepositoryInterface $productRepository,
        ProductFactoryInterface $productFactory,
        EventDispatcherInterface $dispatcher
    ) {
        $this->productFactory = $productFactory;
        $this->dispatcher = $dispatcher;

        $this->beConstructedWith($productRepository, $productFactory, $dispatcher);
    }

    function it_should_call_event_product_created()
    {
        $productDTO = ProductDTO::createNew();
        $command = new CreateProductCommand($productDTO);
        $this->productFactory->fromDTO($command->getProductDTO())->willReturn(new Product());

        $event = new ProductCreatedEvent(new Product());
        $this->dispatcher->dispatch($event)->shouldBeCalledOnce();

        $this($command);
    }
}
