<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Handler;

use App\Component\Product\Application\Exception\ProductNotFoundException;
use App\Component\Product\Application\Message\GetProductQuery;
use App\Component\Product\Domain\Entity\Product;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;

class GetProductHandler
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(GetProductQuery $query): void
    {
        $product = $this->productRepository->getProduct($query->getProductId());
        if(!$product instanceof Product) {
            throw new ProductNotFoundException(
                sprintf('Product with id: %d not found', $query->getProductId())
            );
        }

        $query->setView($product);
    }
}