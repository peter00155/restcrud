<?php

declare(strict_types=1);

namespace App\Shared\Application\Symfony\Request\Factory;

use App\Shared\Application\Symfony\Request\DTO\RequestListDTO;
use Symfony\Component\HttpFoundation\Request;

interface RequestListDTOFactoryInterface
{
    public function fromRequest(Request $request, array $allowedColumns): RequestListDTO;
}