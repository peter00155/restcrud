<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Message;

use App\Component\Product\Application\DTO\ProductDTO;

class CreateProductCommand
{
    private ProductDTO $productDTO;

    public function __construct(ProductDTO $productDTO)
    {
        $this->productDTO = $productDTO;
    }

    public function getProductDTO(): ProductDTO
    {
        return $this->productDTO;
    }
}