<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Service;

use App\Component\Product\Application\DTO\CurrencyDTO;
use App\Component\Product\Domain\Entity\Currency;
use App\Component\Product\Domain\Repository\CurrencyRepositoryInterface;

class CurrencyService
{
    private CurrencyRepositoryInterface $currencyRepository;

    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @return array|CurrencyDTO[]
     */
    public function getCurrencyDTOs(): array
    {
        $currencies = $this->currencyRepository->getCurrencies();
        
        return array_map(static function (Currency $currency) {
            return CurrencyDTO::create(
                $currency->getId(),
                $currency->getCode(),
            );
        }, $currencies);
    }
}