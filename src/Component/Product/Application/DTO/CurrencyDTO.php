<?php

declare(strict_types=1);

namespace App\Component\Product\Application\DTO;

class CurrencyDTO
{
    private ?int $id;

    private ?string $code;

    public static function create(int $id, string $code): self
    {
        $dto = new static();
        $dto->setId($id);
        $dto->setCode($code);

        return $dto;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }
}