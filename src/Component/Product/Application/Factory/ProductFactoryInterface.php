<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Factory;

use App\Component\Product\Application\DTO\ProductDTO;
use App\Component\Product\Domain\Entity\Product;

interface ProductFactoryInterface
{
    public function fromDTO(ProductDTO $productDTO): Product;
}