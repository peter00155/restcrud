<?php

declare(strict_types=1);

namespace App\Component\Product\Domain\Repository;

use App\Component\Product\Domain\Entity\Currency;

interface CurrencyRepositoryInterface
{
    /**
     * @return array|Currency[]
     */
    public function getCurrencies(): array;

    public function getCurrency(int $id): Currency;
}