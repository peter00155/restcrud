<?php

namespace App\Component\Product\Domain\Entity;

use App\Component\Product\Infrastructure\Doctrine\Repository\CurrencyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private string $code;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->code = $id;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
