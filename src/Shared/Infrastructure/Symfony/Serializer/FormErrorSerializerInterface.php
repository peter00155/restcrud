<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Symfony\Serializer;

use Symfony\Component\Form\FormInterface;

interface FormErrorSerializerInterface
{
    public function convertFormToArray(FormInterface $data): array;
}