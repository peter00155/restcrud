<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Symfony\Security;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;

class ParameterTokenAuthVoter extends Voter
{
    private ContainerInterface $container;

    private RequestStack $requestStack;

    public const API_ACCESS = 'API_ACCESS';

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $attribute === self::API_ACCESS;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $requestApiToken = $this->requestStack->getCurrentRequest()->headers->get('api-token');
        if (null === $requestApiToken) {
            throw new TokenNotFoundException('No API token provided');
        }

        $validApiToken = $this->container->getParameter('api-token');
        if ($requestApiToken !== $validApiToken) {
            throw new AccessDeniedException('You are no granted');
        }

        return true;
    }
}