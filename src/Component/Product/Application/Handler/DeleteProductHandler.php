<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Handler;

use App\Component\Product\Application\Message\DeleteProductCommand;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;

class DeleteProductHandler
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(DeleteProductCommand $command): void
    {
        $this->productRepository->deleteProduct($command->getProduct());
    }
}