<?php

declare(strict_types=1);

namespace App\Component\Product\Domain\Repository;

use App\Component\Product\Domain\Entity\Product;

interface ProductRepositoryInterface
{
    public function getProduct(int $id): ?Product;

    /**
     * @return array|Product[]
     */
    public function getProducts(int $offset, int $limit, string $column, string $order): array;

    public function deleteProduct(Product $product): void;

    public function saveProduct(Product $product): void;
}