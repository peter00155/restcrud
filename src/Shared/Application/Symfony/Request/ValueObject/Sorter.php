<?php

declare(strict_types=1);

namespace App\Shared\Application\Symfony\Request\ValueObject;

use App\Shared\Application\Symfony\Request\DTO\RequestListDTO;

class Sorter
{
    private ?string $column;

    private ?string $order;

    public function __construct(?string $sort, array $allowedColumns)
    {
        $this->setValues($sort, $allowedColumns);
    }

    public function getColumn(): string
    {
        return $this->column;
    }

    public function getOrder(): string
    {
        return $this->order;
    }

    private function setValues(?string $sort, array $allowedColumns): void
    {
        $sortParts = [];
        if ($sort) {
            $sortParts = explode('.', $sort);
        }

        $this->column = $this->isSortPartValid($sortParts, 0, $allowedColumns) ?
            $sortParts[0] :
            RequestListDTO::DEFAULT_SORT_COLUMN;

        $this->order = $this->isSortPartValid($sortParts, 1, ['ASC', 'DESC', 'asc', 'desc']) ?
            $sortParts[1] :
            RequestListDTO::DEFAULT_SORT_ORDER;
    }

    private function isSortPartValid(array $sortParts, int $key, array $allowedValues): bool
    {
        return isset($sortParts[$key]) && in_array($sortParts[$key], $allowedValues, true);
    }
}