<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Message;

use App\Shared\Application\Symfony\Request\DTO\RequestListDTO;

class GetProductListQuery
{
    private array $view = [];

    private RequestListDTO $requestListDTO;

    public function __construct(RequestListDTO $requestListDTO)
    {
        $this->requestListDTO = $requestListDTO;
    }

    public function getRequestListDTO(): RequestListDTO
    {
        return $this->requestListDTO;
    }

    public function setView(array $products): void
    {
        $this->view = $products;
    }

    public function getView(): array
    {
        return $this->view;
    }
}