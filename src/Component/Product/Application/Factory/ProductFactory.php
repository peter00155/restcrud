<?php

declare(strict_types=1);

namespace App\Component\Product\Application\Factory;

use App\Component\Product\Application\DTO\ProductDTO;
use App\Component\Product\Domain\Entity\Currency;
use App\Component\Product\Domain\Entity\Product;
use App\Component\Product\Domain\Repository\CurrencyRepositoryInterface;
use App\Component\Product\Domain\Repository\ProductRepositoryInterface;

class ProductFactory implements ProductFactoryInterface
{
    private ProductRepositoryInterface $productRepository;
    private CurrencyRepositoryInterface $currencyRepository;

    public function __construct(ProductRepositoryInterface $productRepository, CurrencyRepositoryInterface $currencyRepository)
    {
        $this->productRepository = $productRepository;
        $this->currencyRepository = $currencyRepository;
    }

    public function fromDTO(ProductDTO $productDTO): Product
    {
        $currency = $this->currencyRepository->getCurrency($productDTO->getCurrency()->getId());

        $product = new Product();
        if (null !== $productDTO->getId()) {
            $product = $this->productRepository->getProduct($productDTO->getId());
        }
        $product->setName($productDTO->getName());
        $product->setDescription($productDTO->getDescription());
        $product->setPrice($productDTO->getPrice());
        $product->setCurrency($currency);

        return $product;
    }
}