<?php

declare(strict_types=1);

namespace App\Component\Product\Infrastructure\Symfony\Form;

use App\Component\Product\Application\DTO\ProductDTO;
use App\Component\Product\Application\Service\CurrencyService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    private CurrencyService $currencyService;

    private RequestStack $requestStack;

    public function __construct(
        CurrencyService $currencyService,
        RequestStack $requestStack
    ) {
        $this->currencyService = $currencyService;
        $this->requestStack = $requestStack;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('description', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Length(['min' => 10]),
                    new NotBlank()
                ],
            ])
            ->add('currency', ChoiceType::class, [
                'choices' => $this->currencyService->getCurrencyDTOs(),
                'choice_value' => 'code',
                'invalid_message' => 'This value is not valid. Consider to load data-fixture `bin/console doctrine:fixtures:load` or add currency in DB first '
            ])
            ->add('price', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThan(0)
                ],
            ])->addEventListener(
                FormEvents::PRE_SUBMIT,
                [$this, 'onPreSubmit']
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => ProductDTO::class,
        ]);
    }

    public function onPreSubmit(FormEvent $event): void
    {
        if(!$this->requestStack->getCurrentRequest()->isMethod('PATCH')) {
            return;
        }

        $oldData = $event->getForm()->getData()->toArray();
        $data = $event->getData() + $oldData;
        unset($data['id']);

        $event->setData($data);
    }
}